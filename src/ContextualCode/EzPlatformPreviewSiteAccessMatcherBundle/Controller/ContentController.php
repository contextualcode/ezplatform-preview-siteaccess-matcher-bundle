<?php

namespace ContextualCode\EzPlatformPreviewSiteAccessMatcherBundle\Controller;

use eZ\Bundle\EzPublishCoreBundle\DependencyInjection\Configuration\ChainConfigResolver;
use eZ\Publish\API\Repository\ContentService;
use eZ\Publish\API\Repository\LocationService;
use eZ\Publish\API\Repository\Values\Content\Content;
use eZ\Publish\API\Repository\Values\Content\Location;
use eZ\Publish\Core\Base\Exceptions\BadStateException;
use eZ\Publish\Core\Base\ServiceContainer;
use EzSystems\EzPlatformAdminUi\Siteaccess\SiteaccessResolverInterface;
use EzSystems\EzPlatformAdminUiBundle\Controller\Controller as BaseController;
use Symfony\Component\HttpFoundation\Response;

class ContentController extends BaseController
{

    /** @var ContentService */
    private $contentService;

    /** @var SiteaccessResolverInterface */
    private $siteaccessResolver;

    /** @var LocationService */
    private $locationService;

    /** @var ChainConfigResolver */
    protected $configResolver;

    protected $container;

    /** @var array */
    public static $rootMapping = array();

    /** @var int|bool */
    public static $currentRoot = false;

    /**
     * @param \eZ\Publish\API\Repository\ContentService $contentService
     * @param \EzSystems\EzPlatformAdminUi\Siteaccess\SiteaccessResolverInterface $siteaccessResolver
     * @param \eZ\Publish\API\Repository\LocationService $locationService
     * @param ChainConfigResolver
     * @param ServiceContainer
     */
    public function __construct(
        ContentService $contentService,
        SiteaccessResolverInterface $siteaccessResolver,
        LocationService $locationService,
        ChainConfigResolver $configResolver
    ) {
        $this->contentService = $contentService;
        $this->siteaccessResolver = $siteaccessResolver;
        $this->locationService = $locationService;
        $this->configResolver = $configResolver;
    }

    public function buildRootMapping()
    {
        if (!self::$currentRoot) {
            self::$currentRoot = $this->configResolver->getParameter('content.tree_root.location_id');
        }

        if (count(self::$rootMapping) === 0) {
            $siteaccesses = $this->container->getParameter('ezpublish.siteaccess.list');

            foreach ($siteaccesses as $siteaccess) {
                $rootParam = 'ezsettings.' . $siteaccess . '.content.tree_root.location_id';
                if (!$this->container->hasParameter($rootParam)) {
                    $rootParam = 'ezsettings.default.content.tree_root.location_id';
                }
                if ($this->container->hasParameter($rootParam)) {
                    self::$rootMapping[$siteaccess] = $this->container->getParameter($rootParam);
                }
            }
        }
    }

    /**
     * @param Content $content
     * @param string|null $languageCode
     * @param int|null $versionNo
     * @param Location|null $location
     *
     * @return Response
     */
    public function previewAction(
        Content $content,
        ?string $languageCode = null,
        ?int $versionNo = null,
        ?Location $location = null
    ): Response {
        if (null === $languageCode) {
            $languageCode = $content->contentInfo->mainLanguageCode;
        }

        // nonpublished content should use parent location instead because location doesn't exist yet
        if (!$content->contentInfo->published && null === $content->contentInfo->mainLocationId) {
            $versionInfo = $this->contentService->loadVersionInfo($content->contentInfo, $versionNo);
            $parentLocations = $this->locationService->loadParentLocationsForDraftContent($versionInfo);
            $location = reset($parentLocations);
            $versionNo = null;
        }

        if (null === $location) {
            $location = $this->locationService->loadLocation($content->contentInfo->mainLocationId);
        }

        $siteaccesses = $this->siteaccessResolver->getSiteaccessesForLocation($location, $versionNo, $languageCode);

        if (empty($siteaccesses)) {
            throw new BadStateException(
                'siteaccess',
                'There is no siteaccesses available for particular content'
            );
        }

        // figure out which siteaccess this node is in
        $pathArray = array_filter(explode('/', $location->pathString));
        $siteaccessIntersect = array_intersect(self::$rootMapping, $pathArray);
        $defaultSiteaccess = $siteaccessIntersect ? key($siteaccessIntersect) : reset($siteaccesses);

        return $this->render('@ezdesign/content/content_preview_override.html.twig', [
            'location' => $location,
            'content' => $content,
            'language_code' => $languageCode,
            'default_siteaccess' => $defaultSiteaccess,
            'siteaccesses' => $siteaccesses,
            'version_no' => $versionNo ?? $content->getVersionInfo()->versionNo,
        ]);
    }

}
